---
title: ""
date: 2022-06-08T23:08:31-04:00
draft: true
---

Hi, I'm Jason. I am a computer programmer with a particular interest in
systems programming and communications. I studied computer science at
Drexel University and minored in electrical engineering. I have always
been fascinated by electronics and how they work so naturally, these
subjects interest me. I am a proponent of open-source software and Linux
is my primary system (I use [Arch Linux](https://archlinux.org/) btw 😁).

As a hobby, I like to tinker with electronics. I also maintain a personal
server — a repurposed Windows Vista desktop — I now call _servo_. I
use it for hosting occasional game servers for my friends and for learning
about system administration on linux. Thankfully, working with computers
is mostly free whereas electronic components and equipment are definitely
_not_. When I'm not in front of a computer, I will often be playing
instruments. In order of proficiency, I play the drums, guitar and piano.
